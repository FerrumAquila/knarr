from django.db import models
from game.models import user


class goodlinks(models.Model):
    user_from = models.ForeignKey(user, related_name = 'user_from')
    user_to = models.ForeignKey(user, related_name = 'user_to')
    url = models.URLField('Goodlink URL', unique = True)
    site_name = models.CharField('Site Name', max_length = 100)

class message(models.Model):
    user_from = models.ForeignKey(user, related_name = 'user_from')
    user_to = models.ForeignKey(user, related_name = 'user_to')
    text = models.CharField('Message', max_length = 160)
__author__ = 'ironeagle'

from django.shortcuts import *
from django.http import *
from django.contrib.auth import logout, login
from django.contrib.auth.decorators import login_required
from django import forms
from django.contrib.auth import authenticate
from itertools import chain
from datetime import *
import json
from django.utils import timezone
from random import randint
from django.core import serializers
from itertools import takewhile
from django.core.mail import send_mail
from django.core.mail import EmailMultiAlternatives
from django.core.mail import EmailMessage
from allauth.socialaccount.models import SocialAccount
from twitter_clone.models import *
from twitter_clone.utils import sign_up_or_login
from django.contrib.auth import login as auth_login
from django.http import Http404



def tc_follow(request, username=None):
    response = {}

    print("==========================================================")
    print str(username) + " followed by " + str(request.user.username)

    if username == None:
        print("==========================================================")
        print "username is None"
        response.update({
            "status": "error",
            "message": "please tell whom to follow",
        })
        response = (json.dumps(response))
        print("==========================================================")
        print response

    elif request.user.is_authenticated() == False:
        print("==========================================================")
        print "no user"
        response.update({
            "status": "error",
            "message": "please login before following",
        })
        response = (json.dumps(response))
        print("==========================================================")
        print response

    else:
        print("==========================================================")
        print "follow ok"
        follower = request.user.profile.all()[0]
        print("==========================================================")
        print "follower " + str(follower)
        following = ProfileDetails.objects.get(username=username)
        print("==========================================================")
        print "following " + str(following)
        follow_user = FollowUser(following=following, follower=follower)
        follow_user.save()
        print("==========================================================")
        print str(follow_user.following.username) + " followed by " + str(follow_user.follower.username)
        response.update({
            "status": "success",
            "message": "FollowUser ID " + str(follow_user.id) + " successful",
        })
        print("==========================================================")
        response = json.dumps(response)
        print response

    return HttpResponse(response)


def tc_like_tweet(request, tweet_id=None):
    response = {}

    if tweet_id == None:
        print("==========================================================")
        print "tweet_id is None"
        response.update({
            "status": "error",
            "message": "please tell which tweet to like",
        })
        response = (json.dumps(response))
        print("==========================================================")
        print response

    elif request.user.is_authenticated() == False:
        print("==========================================================")
        print "no user"
        response.update({
            "status": "error",
            "message": "please login before likeing",
        })
        response = (json.dumps(response))
        print("==========================================================")
        print response

    else:
        print("==========================================================")
        print "like ok"
        liker = request.user.profile.all()[0]
        non_likable_tweets = [tweet_like.tweet for tweet_like in liker.liked_tweets.all()]
        print("==========================================================")
        print "replier " + str(liker)
        liked_tweet = Tweet.objects.get(id=tweet_id)
        print("==========================================================")
        print "liking " + str(liked_tweet.id)
        try:
            liked_tweet.add_liked_by(liker)
            print("==========================================================")
            print "liked tweet ID " + str(liked_tweet.id) + " liked by " + str(liked_tweet.liked_by_profiles.all().order_by('-id')[0].liked_by.username) + " successfully"
            response.update({
                "status": "success",
                "message": "Tweet ID " + str(liked_tweet.id) + " successfully liked by " + str(liked_tweet.liked_by_profiles.get(liked_by=liker).liked_by.username)
            })
        except:
            print("==========================================================")
            print "liked tweet ID " + str(liked_tweet.id) + " cannot be liked again by" + str(liked_tweet.liked_by_profiles.get(liked_by=liker).liked_by.username)
            response.update({
                "status": "error",
                "message": "Tweet ID " + str(liked_tweet.id) + " cannot be liked again by " + str(liked_tweet.liked_by_profiles.get(liked_by=liker).liked_by.username)
            })

        print("==========================================================")
        response = json.dumps(response)
        print response

    return HttpResponse(response)


def tc_reply_tweet(request, tweet_id=None):
    response = {}

    reply_tweet_text = request.POST["tweet_text"]
    print("==========================================================")
    print str(tweet_id) + " tweet got reply " + str(reply_tweet_text) + " by " + str(request.user.username)

    if request.user.is_authenticated() == False:
        print("==========================================================")
        print "no user"
        response.update({
            "status": "error",
            "message": "please login before tweeting",
        })
        response = (json.dumps(response))
        print("==========================================================")
        print response
        return HttpResponse(response)

    elif tweet_id == None:
        print("==========================================================")
        print "tweet_id is None"
        response.update({
            "status": "error",
            "message": "please tell which tweet to reply",
        })
        response = (json.dumps(response))
        print("==========================================================")
        print response
        return HttpResponse(response)

    elif reply_tweet_text == "":
        print("==========================================================")
        print "no reply"
        response.update({
            "status": "error",
            "message": "please tell what to reply",
        })
        response = (json.dumps(response))
        print("==========================================================")
        print response
        return HttpResponse(response)

    else:
        tweeter_profile = request.user.profile.all()[0]
        print("==========================================================")
        print "tweeter's profile " + str(tweeter_profile)
        tweet_text = request.POST.get("tweet_text")
        print("==========================================================")
        print "tweeter's tweet " + str(tweet_text)
        tweet_author = tweeter_profile
        print("==========================================================")
        print "tweeter's author " + str(tweet_author)    
        tweet_tags = request.POST.getlist("tweet_tags")
        if tweet_tags:
            print("==========================================================")
            print "tweeter's tags "
            for tweet_tag in tweet_tags:
                print tweet_tag

        reply_tweet = Tweet(text=tweet_text, author=tweet_author)
        reply_tweet.save()
        for tweet_tag in tweet_tags:
            reply_tweet.tags.add(ProfileDetails.objects.get(username=tweet_tag))
        reply_tweet.save()

        ##Replying To Parent - Start
        parent_tweet = Tweet.objects.get(pk=tweet_id)
        print("==========================================================")
        print "replied to tweet " + str(parent_tweet.id)

        tweet_reply = TweetReply(parent_tweet=parent_tweet, reply_tweet=reply_tweet)
        tweet_reply.save()
        ##Replying To Parent - End

        response.update({
            "status": "success",
            "message": "Tweet ID " + str(tweet_reply.parent_tweet.id) + " successfully replied by Tweet ID " + str(tweet_reply.reply_tweet.id),
        })
        print("==========================================================")
        response = json.dumps(response)
        print response

    return HttpResponse(response)


def tc_post_tweet(request):
    response = {}

    post_tweet_text = request.POST["tweet_text"]
    print("==========================================================")
    print str(tweet_id) + " tweet got reply " + str(post_tweet) + " by " + str(request.user.username)

    if request.user.is_authenticated() == False:
        print("==========================================================")
        print "no user"
        response.update({
            "status": "error",
            "message": "please login before tweeting",
        })
        response = (json.dumps(response))
        print("==========================================================")
        print response
        return HttpResponse(response)

    elif post_tweet_text == "":
        print("==========================================================")
        print "no reply"
        response.update({
            "status": "error",
            "message": "please tell what to post",
        })
        response = (json.dumps(response))
        print("==========================================================")
        print response
        return HttpResponse(response)

    else:
        tweeter_profile = request.user.profile.all()[0]
        print("==========================================================")
        print "tweeter's profile " + str(tweeter_profile)
        tweet_text = request.POST.get("tweet_text")
        print("==========================================================")
        print "tweeter's tweet " + str(tweet_text)
        tweet_author = tweeter_profile
        print("==========================================================")
        print "tweeter's author " + str(tweet_author)    
        tweet_tags = request.POST.getlist("tweet_tags")
        if tweet_tags:
            print("==========================================================")
            print "tweeter's tags "
            for tweet_tag in tweet_tags:
                print tweet_tag

        new_tweet = Tweet(text=tweet_text, author=tweet_author)
        new_tweet.save()
        for tweet_tag in tweet_tags:
            new_tweet.tags.add(ProfileDetails.objects.get(username=tweet_tag))
        new_tweet.save()

        response.update({
            "status": "success",
            "message": "Tweet ID " + str(new_tweet.id) + " by " + str(new_tweet.author.username),
        })
        print("==========================================================")
        response = json.dumps(response)
        print response

    return HttpResponse(response)


def tc_landing_page(request):
    context = {}
    print("==========================================================")
    print "landing page"

    return render(request, "tc-login.html", context)


def tc_login(request):
    context = {}
    print("==========================================================")
    print "login"

    login_email = request.POST["login_email"]
    login_password = request.POST["login_password"]
    print("==========================================================")
    print str(login_email) + " " + str(login_password)

    user = sign_up_or_login(email=login_email, password=login_password)
    if user is not None:
        if user.is_active:
            auth_login(request, user)
            return redirect('tc_home')
        else:
            return HttpResponse("<strong>Error</strong> User account has not been activated..")
    else:
        return HttpResponse("<strong>Error</strong> Username or password was incorrect.")

    #return render_to_response('accounts/login.html', {}, context_instance=RequestContext(request))

    #return render(request, "tc-login.html", context)


def tc_home(request):
    context = {}
    print("==========================================================")
    print "home"

    if request.user.is_authenticated():
        user_profile = request.user.profile.filter(username=str(request.user.username))
        if user_profile.count():
            print("==========================================================")
            print "Username '" + str(user_profile[0].username) + "' Found"
        else:
            new_profile = ProfileDetails(username=request.user.username, user=request.user, email=request.user.email, gender="U")
            new_profile.save()
            user_profile = []
            user_profile.append(new_profile)

        user_profile = user_profile[0]
        context.update({
            "user_profile": user_profile,
        })

        feed = NewsFeed(listener=user_profile)
        feed.save()
        print("==========================================================")
        print "feed " + str(len(feed.populate_feed()))
        context.update({
            "feed": Tweet.objects.filter(id__in=feed.populate_feed()).order_by("-created_at"),
        })

        if user_profile.get_follow_suggestion().count() >= 6:
            follow_suggestions = user_profile.get_follow_suggestion()[0:6]
            print("==========================================================")
            print "suggestions " + str(user_profile.get_follow_suggestion().count())
            context.update({
                "follow_suggestions": follow_suggestions,
            })
        elif user_profile.get_follow_suggestion().count() < 6:
            follow_suggestions = user_profile.get_follow_suggestion()[:]
            print("==========================================================")
            print "suggestions " + str(user_profile.get_follow_suggestion().count())
            context.update({
                "follow_suggestions": follow_suggestions,
            })
        else:
            print("==========================================================")
            print "suggestions not found"

        if len(user_profile.get_taggable_profiles()):
            taggable_profiles = user_profile.get_taggable_profiles()
            print("==========================================================")
            print "taggable profiles " + str(len(taggable_profiles))
            context.update({
                "taggable_profiles": taggable_profiles,
            })
        else:
            print("==========================================================")
            print "taggable_profiles not found"

    else:
        return redirect("tc_landing_page")

    return render(request, "tc-home.html", context)


def tc_timeline(request, username=None):
    context = {}

    if username:
        user_profile = ProfileDetails.objects.get(username=username)
        print("==========================================================")
        print "profile with username " + str(username)
    elif request.user.is_authenticated():
        user_profile = request.user.profile.get(username=str(request.user.username))
        print("==========================================================")
        print "logged in with username " + str(user_profile.username)
    else:
        raise Http404

    context.update({
        "user_profile": user_profile,
    })

    user_timeline = Timeline(tweeter=user_profile)
    user_timeline.save()
    print("==========================================================")
    print "timeline " + str(len(user_timeline.populate_timeline()))
    context.update({
        "timeline": Tweet.objects.filter(id__in=user_timeline.populate_timeline()).order_by("-created_at"),
    })

    return render(request, "tc-profile.html", context)


def tc_tweet(request, tweet_id=None):
    context = {}
    print("==========================================================")
    print "tweet feed " + str(tweet_id)

    if tweet_id == None:
        print("==========================================================")
        print "tweet_id is None"
    else:
        tweet = Tweet.objects.get(id=tweet_id)

        print("==========================================================")
        print "tweet ID " + str(tweet.id) + " found"
        context.update({
            "tweet": tweet,
        })

        tweet_feed = tweet.parent_to.all()
        print("==========================================================")
        print "tweet feed " + str(tweet_feed.count())
        context.update({
            "reply_tweet_feed": tweet_feed,
        })

    if request.user.is_authenticated():

        user_profile = request.user.profile.get(username=str(request.user.username))
        print("==========================================================")
        print "Username '" + str(user_profile.username) + "' Found"
        if len(user_profile.get_taggable_profiles()):
            taggable_profiles = user_profile.get_taggable_profiles()
            print("==========================================================")
            print "taggable profiles " + str(len(taggable_profiles))
            context.update({
                "taggable_profiles": taggable_profiles,
            })
        else:
            print("==========================================================")
            print "taggable_profiles not found"

    return render(request, "tc-tweet.html", context)


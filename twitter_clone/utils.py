__author__ = 'ironeagle'

from django.contrib.auth.models import User
from django.contrib.auth import authenticate


def populate_listeners_feed(listener, return_tweets=10):
    following = listener.follows.all()

    if following.count() != 0:
        per_author_tweets = int(int(return_tweets)/int(following.count()))
    else:
        return []

    if per_author_tweets:
        pass
    else:
        per_author_tweets = 1

    tweet_sets = [follow.following.tweets.all().order_by('-id')[0:per_author_tweets] for follow in following]
    feed_tweets_ids = []
    for tweet_set in tweet_sets:
        for tweet in tweet_set:
            feed_tweets_ids.append(tweet.id)
    return feed_tweets_ids


def populate_tweeters_timeline(tweeter, return_tweets=10):

    own_tweets = tweeter.tweets.all()
    tagged_tweets = tweeter.tagged_in.all()
    total_tweets = own_tweets | tagged_tweets
    total_tweets_ids = list(set([tweet.id for tweet in total_tweets.order_by("id")]))
    return total_tweets_ids[0:return_tweets]


def _is_valid_email(email):
    from django.core.validators import validate_email
    from django.core.exceptions import ValidationError
    try:
        validate_email(email)
        return True
    except ValidationError:
        return False


def sign_up_or_login(email, password):
    #username = email
    password = password

    try_user = User.objects.filter(email=email)
    if try_user.count():
        pass
    else:
        new_username = email.split("@")[0]
        stripped_username = ""
        for element in new_username.split("."):
            stripped_username += element
        new_username = stripped_username
        new_email = email
        new_user = User(email=new_email, username=new_username)
        new_user.save()
        new_user.set_password(password)
        new_user.save()

    if _is_valid_email(email):
        try:
            username = User.objects.filter(email=email).values_list('username', flat=True)
        except User.DoesNotExist:
            username = None
        kwargs = {'username': username, 'password': password}
        try:
            user = authenticate(**kwargs)
        except:
            user = None

        return user
    else:
        return None
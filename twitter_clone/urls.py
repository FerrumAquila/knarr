
from django.conf.urls import patterns, include, url


urlpatterns = patterns('twitter_clone.views',
    #Temporary ones
   # url(r'^home/$', 'home', name='test_platform_home'),
    #url(r'^take-test/$', 'take_test', name='test_platform_take_test'),
   # url(r'^take-test/(?P<testid>\d+)$', 'take_this_test', name='test_platform_take_this_test'),
    #url(r'^start-test/$','start_test'),
    #url(r'^start-test/check-timer/$','check_timer'),

    #url(r'^add-test/$', 'add_test', name='test_platform_add_tests'),
    #url(r'^add-section/$', 'add_section', name='test_platform_add_section'),
    #url(r'^add-questions/$', 'add_questions', name='test_platform_add_questions'),
    #url(r'^generate-questions/(?P<typeofquestion>[-\w]+)/(?P<noofquestion>\d+)/$', 'generate_random_questions', name='test_platform_random_questions'),
    #url(r'^add-test/save-section', 'save_section', name='test_platform_save_section'),

    #url(r'^calc-score/$', 'calc_score'),

    #Main urls for viewing the templates
    #url(r'^', 'tc_home', name='tc_home'),
    url(r'^follow$', 'tc_follow', name='tc_follow'),
    url(r'^follow/(?P<username>\w+)$', 'tc_follow', name='tc_follow'),
    url(r'^home$', 'tc_home', name='tc_home'),
    url(r'^landing_page$', 'tc_landing_page', name='tc_landing_page'),
    url(r'^login$', 'tc_login', name='tc_login'),
    url(r'^like_tweet$', 'tc_like_tweet', name='tc_like_tweet'),
    url(r'^like_tweet/(?P<tweet_id>\w+)$', 'tc_like_tweet', name='tc_like_tweet'),
    url(r'^timeline$', 'tc_timeline', name='tc_timeline'),
    url(r'^timeline/(?P<username>\w+)$', 'tc_timeline', name='tc_timeline'),
    url(r'^post_tweet$', 'tc_post_tweet', name='tc_post_tweet'),
    url(r'^reply_tweet$', 'tc_reply_tweet', name='tc_reply_tweet'),
    url(r'^reply_tweet/(?P<tweet_id>\w+)$', 'tc_reply_tweet', name='tc_reply_tweet'),
    url(r'^tweet$', 'tc_tweet', name='tc_tweet'),
    url(r'^tweet/(?P<tweet_id>\w+)$', 'tc_tweet', name='tc_tweet'),
    )

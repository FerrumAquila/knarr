from django.contrib import admin
from game.models import user, comment, arena


class commentInline(admin.TabularInline):
    model = comment
    extra = 1


class userAdmin(admin.ModelAdmin):
    fieldsets = [
        ('Details', {'fields': ['name', 'call_sign', 'dob', 'email']}),
        ('Information', {'fields': ['info']}), #, 'joined_date', 'last_seen'
        ('Password', {'fields': ['password']}),
        ('Stats', {'fields': ['total_wins', 'total_losses', 'total_draw', 'player_number', 'session_number']})
    ]
    inlines = [commentInline]
    list_display = ('name', 'call_sign', 'dob', 'password', 'last_seen', 'joined_date', 'session_number', 'total_wins', 'total_losses', 'total_draw')
    list_filter = ('dob', 'last_seen', 'joined_date')
    search_field = ('name', 'call_sign', 'dob', 'info', 'email')


class commentAdmin(admin.ModelAdmin):
    fieldsets = [
        ('Commented By', {'fields': ['user']}),
        ('Message', {'fields': ['text']})
    ]
    list_display = ('user', 'text', 'comment_time')
    list_filter = ('comment_time', 'user')
    search_field = ('text', 'user')


class arenaAdmin(admin.ModelAdmin):
    fieldsets = [
        ('Battle Arena', {'fields': ['user', 'created_date']}),
        ('', {'fields': ['cell11', 'cell12', 'cell13']}),
        ('', {'fields': ['cell21', 'cell22', 'cell23']}),
        ('', {'fields': ['cell31', 'cell32', 'cell33']})
    ]
    list_display = ('user', 'created_date')

admin.site.register(user, userAdmin)
admin.site.register(comment, commentAdmin)
admin.site.register(arena, arenaAdmin)
import random
from django.http import HttpResponse
from django.shortcuts import render, redirect
from django.utils import timezone

from game.models import user, comment, arena, RayBanGlasses


def home(request):
    #return HttpResponse("site for homepage")
    all_users = user.objects.order_by('-last_seen')[:5]
    login_numbers = user.objects.order_by('-session_number')[:5]
    try:
        cs = request.COOKIES["logged_call_sign"]
        logged_user = user.objects.get(call_sign=cs)
        context = {'logged_user': logged_user, 'all_users': all_users, 'login_numbers': login_numbers}
        #return HttpResponse(logged_user.call_sign)
    except:
        context = {'all_users': all_users, 'login_numbers': login_numbers}
    return render(request, 'game/home.html', context)


def resume(request):
    #return HttpResponse("site for homepage")
    return render(request, 'My_CV.html')


def happy_birthday(request, name='Vishesh'):
    #return HttpResponse("happy_birthday")
    wishes = [
        "May your birthday and every day be filled with the warmth of sunshine, the happiness of smiles, the sounds of laughter, the feeling of love and the sharing of good cheer.",
        "I hope you have a wonderful day and that the year ahead is filled with much love, many wonderful surprises and gives you lasting memories that you will cherish in all the days ahead. Happy Birthday.",
        "On this special day, i wish you all the very best, all the joy you can ever have and may you be blessed abundantly today, tomorrow and the days to come! May you have a fantastic birthday and many more to come... HAPPY BIRTHDAY!!!!",
        "They say that you can count your true friends on 1 hand - but not the candles on your birthday cake! #1Happybirthday",
        "Celebrate your birthday today. Celebrate being Happy every day.",
        "May your birthday be filled with many happy hours and your life with many happy birthdays. HAPPY BIRTHDAY !!"]
    wish = random.sample(wishes, 1)
    wish = wish[0]
    context = {
        'name': name,
        'wish': wish
    }
    return render(request, 'happy_birthday.html', context)


def user_profile(request, user_id):
    all_users = user.objects.order_by('-last_seen')[:5]
    login_numbers = user.objects.order_by('-session_number')[:5]
    try:
        cs = request.COOKIES["logged_call_sign"]
        logged_user = user.objects.get(call_sign=cs)
        profile_user = user.objects.get(call_sign=user_id)
        context = {'profile_user': profile_user, 'logged_user': logged_user, 'all_users': all_users,
                   'login_numbers': login_numbers}
    except:
        profile_user = user.objects.get(call_sign=user_id)
        context = {'profile_user': profile_user, 'all_users': all_users, 'login_numbers': login_numbers}
    return render(request, 'game/profile.html', context)


def chat(request):
    #return HttpResponse("landing page after comment sent\ncurrent user %s" % user_id)
    all_users = user.objects.order_by('-last_seen')[:5]
    login_numbers = user.objects.order_by('-session_number')[:5]
    all_comments = comment.objects.all()
    try:
        cs = request.COOKIES["logged_call_sign"]
        logged_user = user.objects.get(call_sign=cs)
        context = {'logged_user': logged_user, 'login_numbers': login_numbers, 'all_chats': all_comments,
                   'all_users': all_users}
    #return HttpResponse(sending_user.call_sign)
    except:
        context = {'login_numbers': login_numbers, 'all_chats': all_comments, 'all_users': all_users}
    return render(request, 'game/chat_area.html', context)


def sending_chat(request):
    #return HttpResponse("landing page after comment sent\ncurrent user %s" % user_id)
    #text = request.POST.get("comment")
    #return HttpResponse(text)
    try:
        cs = request.COOKIES["logged_call_sign"]
        sending_user = user.objects.get(call_sign=cs)
    #return HttpResponse(sending_user.call_sign)
    except:
        return render(request, 'game/login.html')
    sending_user.last_seen = timezone.now()
    sending_user.save()
    new_comment = comment(user=sending_user, text=request.POST.get("comment"), comment_time=timezone.now())
    new_comment.save()
    request.POST.get("comment")
    all_comments = comment.objects.all()
    context = {'sending_user': sending_user, 'all_chats': all_comments}
    return render(request, 'game/chat_sent.html', context)


def add_user(request):
    all_users = user.objects.order_by('-last_seen')[:5]
    login_numbers = user.objects.order_by('-session_number')[:5]
    try:
        cs = request.COOKIES["logged_call_sign"]
        context = {'all_users': all_users, 'login_numbers': login_numbers}
        response = render(request, 'game/add_user.html', context)
        response.set_cookie("logged_call_sign", "")
    except:
        context = {'all_users': all_users, 'login_numbers': login_numbers}
        response = render(request, 'game/add_user.html', context)
    return response


def user_added(request):
    name = request.POST.get("name")
    call_sign = request.POST.get("call_sign")
    info = request.POST.get("info")
    dob = request.POST.get("dob")
    password = request.POST.get("password")
    email = request.POST.get("email")
    new_user = user(name=name, call_sign=call_sign, info=info, dob=dob, password=password, total_wins=0, total_losses=0,
                    total_draw=0, player_number=0, session_number=0, email=email)
    new_user.save()
    all_users = user.objects.order_by('-last_seen')[:5]
    login_numbers = user.objects.order_by('-session_number')[:5]
    logged_user = new_user
    context = {'new_user': new_user, 'logged_user': logged_user, 'login_numbers': login_numbers, 'all_users': all_users}
    response = render(request, 'game/user_added.html', context)
    response.set_cookie("logged_call_sign", request.POST.get("call_sign"))
    return response


def login(request):
    all_users = user.objects.order_by('-last_seen')[:5]
    login_numbers = user.objects.order_by('-session_number')[:5]
    try:
        cs = request.COOKIES["logged_call_sign"]
        context = {'login_numbers': login_numbers, 'all_users': all_users}
        response = render(request, 'game/login.html', context)
        response.set_cookie("logged_call_sign", "")
    except:
        context = {'all_users': all_users, 'login_numbers': login_numbers}
        response = render(request, 'game/login.html', context)
    return response


def check_login(request):
    #call_sign = request.POST.get("call_sign")	#call the login user by name (redundant)
    all_users = user.objects.all()
    login_user = user.objects.get(call_sign=request.POST.get("call_sign"))
    context = {'all_users': all_users, 'login_user': login_user}
    if login_user.password == request.POST.get("password"):
        logged_user = user.objects.get(pk=login_user.id)
        logged_user.session_number += 1
        logged_user.save()
        response = render(request, 'game/loginsuccess.html', context)
        response.set_cookie("logged_call_sign", request.POST["call_sign"])
        return response
    else:
        return render(request, 'game/login.html', context)


def signout(request):
    #login_user = user.objects.get(pk = user_id)
    #context = {'login_user' : login_user}
    #proper login screen
    all_users = user.objects.order_by('-last_seen')[:5]
    login_numbers = user.objects.order_by('-session_number')[:5]
    try:
        cs = request.COOKIES["logged_call_sign"]
        context = {'all_users': all_users, 'login_numbers': login_numbers}
        response = render(request, 'game/home.html', context)
        response.set_cookie("logged_call_sign", "")
    except:
        context = {'all_users': all_users, 'login_numbers': login_numbers}
        response = render(request, 'game/home.html', context)
    return response


def screen(request):
    all_users = user.objects.order_by('-last_seen')[:5]
    #login_numbers = user.objects.order_by('-session_number')[:5]
    try:
        cs = request.COOKIES["logged_call_sign"]
        logged_user = user.objects.get(call_sign=cs)
        leng = logged_user.arena_set.count()
        login_numbers = logged_user.arena_set.order_by('-created_date')[:leng / 2]
        context = {'logged_user': logged_user, 'all_users': all_users, 'login_numbers': login_numbers}
        #return HttpResponse(logged_user.call_sign)
    except:
        login_numbers = user.objects.order_by('-session_number')[:5]
        context = {'all_users': all_users, 'login_numbers': login_numbers}
    return render(request, 'game/screen.html', context)


def compare(a1, a2):
    if a1 == '3':
        if a2 == '1':
            return 1
        elif a2 == '2':
            return -1
        elif a2 == '3':
            return 0
    elif a1 == '1':
        if a2 == '1':
            return 0
        elif a2 == '2':
            return 1
        elif a2 == '3':
            return -1
    elif a1 == '2':
        if a2 == '1':
            return -1
        elif a2 == '2':
            return 0
        elif a2 == '3':
            return 1


def save_arena(request):
    pop = ['1', '2', '3']
    all_users = user.objects.order_by('-last_seen')[:5]
    #login_numbers = user.objects.order_by('-session_number')[:5]
    try:
        cs = request.COOKIES["logged_call_sign"]
        logged_user = user.objects.get(call_sign=cs)
        leng = logged_user.arena_set.count()
        login_numbers = logged_user.arena_set.order_by('-created_date')[:leng / 2]
        #context = {'logged_user': logged_user, 'all_users': all_users, 'login_numbers': login_numbers}
    except:
        login_numbers = user.objects.order_by('-session_number')[:5]
        context = {'all_users': all_users, 'login_numbers': login_numbers}
        return render(request, "game/login.html", context)
    cell11 = request.POST.get("Cell11")
    cell12 = request.POST.get("Cell12")
    cell13 = request.POST.get("Cell13")
    cell21 = request.POST.get("Cell21")
    cell22 = request.POST.get("Cell22")
    cell23 = request.POST.get("Cell23")
    cell31 = request.POST.get("Cell31")
    cell32 = request.POST.get("Cell32")
    cell33 = request.POST.get("Cell33")
    new_arena = arena(user=logged_user, created_date=timezone.now(), cell11=cell11, cell12=cell12, cell13=cell13,
                      cell21=cell21, cell22=cell22, cell23=cell23, cell31=cell31, cell32=cell32, cell33=cell33)
    new_arena.save()
    cell11 = random.sample(pop, 1)
    cell12 = random.sample(pop, 1)
    cell13 = random.sample(pop, 1)
    cell21 = random.sample(pop, 1)
    cell22 = random.sample(pop, 1)
    cell23 = random.sample(pop, 1)
    cell31 = random.sample(pop, 1)
    cell32 = random.sample(pop, 1)
    cell33 = random.sample(pop, 1)
    cell11 = cell11[0]
    cell12 = cell12[0]
    cell13 = cell13[0]
    cell21 = cell21[0]
    cell22 = cell22[0]
    cell23 = cell23[0]
    cell31 = cell31[0]
    cell32 = cell32[0]
    cell33 = cell33[0]
    random_arena = arena(user=logged_user, cell11=cell11, cell12=cell12, cell13=cell13,
                         cell21=cell21, cell22=cell22, cell23=cell23, cell31=cell31, cell32=cell32, cell33=cell33)
    random_arena.save()
    score = 0
    score += int(compare(new_arena.cell11, random_arena.cell11))
    score += int(compare(new_arena.cell11, random_arena.cell11))
    score += int(compare(new_arena.cell11, random_arena.cell11))
    score += int(compare(new_arena.cell11, random_arena.cell11))
    score += int(compare(new_arena.cell11, random_arena.cell11))
    score += int(compare(new_arena.cell11, random_arena.cell11))
    score += int(compare(new_arena.cell11, random_arena.cell11))
    score += int(compare(new_arena.cell11, random_arena.cell11))
    score += int(compare(new_arena.cell11, random_arena.cell11))
    if score > 0:
        logged_user.total_wins += 1
        game = 'win'
    elif score < 0:
        logged_user.total_losses += 1
        game = 'lose'
    else:
        logged_user.total_draw += 1
        game = 'draw'
    logged_user.save()
    context = {'logged_user': logged_user, 'all_users': all_users, 'login_numbers': login_numbers, 'arena': new_arena,
               'random': random_arena, 'game': game}
    return render(request, 'game/screen.html', context)


def view_arena(request, arena_id):
    all_users = user.objects.order_by('-last_seen')[:5]
    #login_numbers = user.objects.order_by('-session_number')[:5]
    a_id = int(arena_id) + 1
    a_id = str(a_id)
    user_arena = arena.objects.get(pk=arena_id)
    random_arena = arena.objects.get(pk=a_id)
    try:
        cs = request.COOKIES["logged_call_sign"]
        logged_user = user.objects.get(call_sign=cs)
        leng = logged_user.arena_set.count()
        login_numbers = logged_user.arena_set.order_by('-created_date')[:leng / 2]
        score = 0
        score += int(compare(user_arena.cell11, random_arena.cell11))
        score += int(compare(user_arena.cell11, random_arena.cell11))
        score += int(compare(user_arena.cell11, random_arena.cell11))
        score += int(compare(user_arena.cell11, random_arena.cell11))
        score += int(compare(user_arena.cell11, random_arena.cell11))
        score += int(compare(user_arena.cell11, random_arena.cell11))
        score += int(compare(user_arena.cell11, random_arena.cell11))
        score += int(compare(user_arena.cell11, random_arena.cell11))
        score += int(compare(user_arena.cell11, random_arena.cell11))
        if score > 0:
            game = 'win'
        elif score < 0:
            game = 'lose'
        else:
            game = 'draw'
        if user_arena.user.call_sign == cs:
            context = {'logged_user': logged_user, 'all_users': all_users, 'login_numbers': login_numbers,
                       'arena': user_arena, 'random': random_arena, 'game': game}
        else:
            context = {'logged_user': logged_user, 'all_users': all_users, 'login_numbers': login_numbers}
            #return HttpResponse(logged_user.call_sign)
    except:
        login_numbers = user.objects.order_by('-session_number')[:5]
        context = {'all_users': all_users, 'login_numbers': login_numbers}
        return render(request, "game/login.html", context)
    return render(request, 'game/screen.html', context)


def kills(request):
    all_users = user.objects.order_by('-last_seen')[:5]
    #login_numbers = user.objects.order_by('-session_number')[:5]
    list_users = user.objects.order_by('-total_wins')
    try:
        cs = request.COOKIES["logged_call_sign"]
        logged_user = user.objects.get(call_sign=cs)
        leng = logged_user.arena_set.count()
        login_numbers = logged_user.arena_set.order_by('-created_date')[:leng / 2]
        context = {'logged_user': logged_user, 'all_users': all_users, 'login_numbers': login_numbers,
                   'list': list_users, 'win': list_users}
        #return HttpResponse(logged_user.call_sign)
    except:
        login_numbers = user.objects.order_by('-session_number')[:5]
        context = {'all_users': all_users, 'login_numbers': login_numbers, 'list': list_users}
    return render(request, 'game/list.html', context)


def losses(request):
    all_users = user.objects.order_by('-last_seen')[:5]
    #login_numbers = user.objects.order_by('-session_number')[:5]
    list_users = user.objects.order_by('-total_losses')
    try:
        cs = request.COOKIES["logged_call_sign"]
        logged_user = user.objects.get(call_sign=cs)
        leng = logged_user.arena_set.count()
        login_numbers = logged_user.arena_set.order_by('-created_date')[:leng / 2]
        context = {'logged_user': logged_user, 'all_users': all_users, 'login_numbers': login_numbers,
                   'list': list_users, 'lose': list_users}
        #return HttpResponse(logged_user.call_sign)
    except:
        login_numbers = user.objects.order_by('-session_number')[:5]
        context = {'all_users': all_users, 'login_numbers': login_numbers, 'list': list_users}
    return render(request, 'game/list.html', context)


def draws(request):
    all_users = user.objects.order_by('-last_seen')[:5]
    #login_numbers = user.objects.order_by('-session_number')[:5]
    list_users = user.objects.order_by('-total_draw')
    try:
        cs = request.COOKIES["logged_call_sign"]
        logged_user = user.objects.get(call_sign=cs)
        leng = logged_user.arena_set.count()
        login_numbers = logged_user.arena_set.order_by('-created_date')[:leng / 2]
        context = {'logged_user': logged_user, 'all_users': all_users, 'login_numbers': login_numbers,
                   'list': list_users, 'draw': list_users}
        #return HttpResponse(logged_user.call_sign)
    except:
        login_numbers = user.objects.order_by('-session_number')[:5]
        context = {'all_users': all_users, 'login_numbers': login_numbers, 'list': list_users}
    return render(request, 'game/list.html', context)


def missing(request):
    return HttpResponse(
        "<h1 style='text-align: center'>What am I missing out on in Life if I dont have a Girlfriend???</h1><h2 style='text-align: center'>TRUST ME</h2><h1 style='text-align: center'>NOTHING!!!!!</h1>")


def rayb(request, count=None):
    context = {}
    buyers = []
    fixed = 1500
    shipping = 900
    total_buyers = RayBanGlasses.objects.all()

    if count:
        count = int(count)
        for i in range(0, count, 1):
            buyer = []
            buyer.append("Buyer " + str(i + 1))
            buyer.append(fixed)
            buyer.append(shipping / count)
            buyer.append(fixed + (shipping / count))
            buyers.append(buyer)
    else:
        for i in range(0, total_buyers.count(), 1):
            buyer = []
            buyer.append(str(total_buyers[i].first_name + " " + total_buyers[i].last_name))
            buyer.append(fixed)
            buyer.append(shipping / total_buyers.count())
            buyer.append(fixed + (shipping / total_buyers.count()))
            buyers.append(buyer)

    context.update({
        'buyers': buyers
    })
    return render(request, 'rayb.html', context)


def register_glasses(request):
    return render(request, 'register_glasses.html')


def get_registered_glasses(request):
    first_name = request.POST.get('first_name')
    last_name = request.POST.get('last_name')
    url = request.POST.get('url')
    glasses, created = RayBanGlasses.objects.get_or_create(first_name=first_name, last_name=last_name, selected=url)
    if created is False:
        return HttpResponse("<h1 style='text-align:center'>" + str(
            glasses.first_name) + ",<br>You have already registered <a href='" + str(
            glasses.selected) + ">this glasses</a></h1>")
    if created:
        return HttpResponse("<h1 style='text-align:center'>" + str(
            glasses.first_name) + ",<br>You have been registered for <a href='" + str(
            glasses.selected) + ">this glasses</a></h1>")

from django.conf.urls import patterns, url, include

from game import views

urlpatterns = patterns('game.views',

url(r'$^', views.home, name = 'home'),
url(r'^home/', views.home, name = 'home'),
url(r'^profile/(?P<user_id>\w+)/', views.user_profile, name = 'profile'),
url(r'^adduser/', views.add_user, name = "add_user"),
url(r'^useradded/', views.user_added, name = "user_added"),
url(r'^login/', views.login, name = "login"),
url(r'^checklogin/', views.check_login, name = "check_login"),
url(r'^chat/', views.chat, name = "chat"),
url(r'^chats/', views.sending_chat, name = "sending_chat"),
url(r'^signout/', views.signout, name = "signout"),
url(r'^screen/', views.screen, name = "screen"),
url(r'^savearena/', views.save_arena, name = "save_arena"),
url(r'^arena/(?P<arena_id>\d+)/', views.view_arena, name = "view_arena"),
url(r'^kills/', views.kills, name = "kills"),
url(r'^losses/', views.losses, name = "losses"),
url(r'^draws/', views.draws, name = "draws"),
url(r'^resume/', views.resume, name = "resume"),
url(r'^online_resume/', views.resume, name = "resume"),
url(r'^happy_birthday/$', views.happy_birthday, name = "happy_birthday"),
url(r'^happy_birthday/(?P<name>\w+)$', views.happy_birthday, name = "happy_birthday"),
url(r'^what-am-I-missing-out-on-in-life-if-I-dont-have-a-girlfriend/', views.missing, name = "missing"),
url(r'^ray-ban-register-glasses/', views.register_glasses, name = "register_glasses"),
url(r'^ray-ban-get-registered-glasses/', views.get_registered_glasses, name = "get_registered_glasses"),
url(r'^ray-ban-sale-contribution-table/$', views.rayb, name = "rayb"),
url(r'^ray-ban-sale-contribution-table/(?P<count>\d+)$', views.rayb, name = "rayb"),
)

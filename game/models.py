from django.db import models
from django.core import files


class user(models.Model):
    def __unicode__(self):
        return self.call_sign
    name = models.CharField('User Name', max_length=100)
    call_sign = models.CharField('Call Sign', max_length=100, unique=True)
    info = models.TextField('Information', max_length=1000)
    dob = models.DateField('Date of Birth')
    email = models.EmailField('Email Address')
    password = models.CharField('Password', max_length=50)
    joined_date = models.DateField('Joined Date', auto_now_add=True)
    last_seen = models.DateTimeField('Last Activity', auto_now=True)
    total_wins = models.IntegerField('Wins')
    total_losses = models.IntegerField('Losses')
    total_draw = models.IntegerField('Draws')
    player_number = models.IntegerField('Player Number', default=0)
    session_number = models.BigIntegerField('Logins')
    cookie = models.FileField(blank=True, upload_to='temp')


class comment(models.Model):
    def __unicode__(self):
        return self.text
    user = models.ForeignKey(user)
    text = models.TextField(max_length=1000)
    comment_time = models.DateTimeField('Comment Time', auto_now_add=True)


class arena(models.Model):
    user = models.ForeignKey(user)
    created_date = models.DateTimeField('Created Date', blank=True, null=True)
    cell11 = models.CharField('Element 11', max_length=1)
    cell12 = models.CharField('Element 12', max_length=1)
    cell13 = models.CharField('Element 13', max_length=1)
    cell21 = models.CharField('Element 21', max_length=1)
    cell22 = models.CharField('Element 22', max_length=1)
    cell23 = models.CharField('Element 23', max_length=1)
    cell31 = models.CharField('Element 31', max_length=1)
    cell32 = models.CharField('Element 32', max_length=1)
    cell33 = models.CharField('Element 33', max_length=1)


class goodreads(models.Model):
    user = models.ForeignKey(user)
    goodread_url = models.URLField('Good Read')
    posting_time = models.DateTimeField('Posting Time', auto_now_add=True)


class RayBanGlasses(models.Model):
    first_name = models.CharField('Buyer First Name', max_length=100)
    last_name = models.CharField('Buyer Last Name', max_length=100)
    selected = models.URLField('Selected Glasses')


from django.conf.urls import patterns, include, url
from django.contrib import admin
admin.autodiscover()
from django.conf import settings
from django.conf.urls.static import static

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'asp.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),

    url(r'^accounts/', include('allauth.urls')),

    url(r'^game/', include('game.urls')),

    url(r'^admin/', include(admin.site.urls)),

    url(r'^lounge/', include('lounge.urls')),

    url(r'^twitter_clone/', include('twitter_clone.urls')),

    url(r'^resume/', include('online_resume.urls', namespace="online_resume")),

    #url(r'^keeda/', include('keeda.urls')),

) + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT) + \
    static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)



from django.conf.urls import patterns, include, url

urlpatterns = patterns('online_resume.views',
    #Temporary ones
   # url(r'^home/$', 'home', name='test_platform_home'),
    #url(r'^take-test/$', 'take_test', name='test_platform_take_test'),
   # url(r'^take-test/(?P<testid>\d+)$', 'take_this_test', name='test_platform_take_this_test'),
    #url(r'^start-test/$','start_test'),
    #url(r'^start-test/check-timer/$','check_timer'),

    #url(r'^add-test/$', 'add_test', name='test_platform_add_tests'),
    #url(r'^add-section/$', 'add_section', name='test_platform_add_section'),
    #url(r'^add-questions/$', 'add_questions', name='test_platform_add_questions'),
    #url(r'^generate-questions/(?P<typeofquestion>[-\w]+)/(?P<noofquestion>\d+)/$', 'generate_random_questions', name='test_platform_random_questions'),
    #url(r'^add-test/save-section', 'save_section', name='test_platform_save_section'),

    #url(r'^calc-score/$', 'calc_score'),

    #Main urls for viewing the templates
    url(r'^$', 'resume', name='resume'),
    url(r'^(?P<resume_user>\w+)$', 'resume', name='resume'),


    )
__author__ = 'ironeagle'

from django.shortcuts import render
from django.shortcuts import redirect
from django.http import *
from django.contrib.auth import logout, login
from django.contrib.auth.decorators import login_required
from django import forms
from django.contrib.auth import authenticate
from itertools import chain
from datetime import *
import json
from django.utils import timezone
from random import randint
from django.core import serializers
from itertools import takewhile
from django.core.mail import send_mail
from django.core.mail import EmailMultiAlternatives
from django.core.mail import EmailMessage
from online_resume.models import *


def resume(request, resume_user="ironeagle"):
    context = {}

    profile = Profile.objects.filter(user=User.objects.get(username=resume_user))[0]

    context.update({
        "profile": profile
    })

    return render(request, "resume.html", context)
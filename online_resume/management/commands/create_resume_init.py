from __future__ import unicode_literals
from django.core.management.base import BaseCommand, CommandError
from django.contrib.auth.models import User, UserManager
from online_resume.models import *
import math
from django.db.models import Q


class Command(BaseCommand):

    def create_resume(self, password="vishesh23"):
        create_super_user = UserManager.create_superuser
        users = User.objects.filter(first_name="Vishesh", last_name="Bangotra")
        if len(users) == 0:
            user = User.objects.create_superuser(email="visheshbangotra@gmail.com", username="ironeagle", password=password)
            user.save()
            print ("User Created wth\nUsername " + str(user.username) + "\nPassword " + str(password) + "\nEmail " + str(user.email))
            user.first_name = "Vishesh"
            print ("First Name " + str(user.first_name) + " Added")
            user.last_name = "Bangotra"
            print ("Last Name " + str(user.last_name) + " Added")
            user.save()
            profile = Profile(user=user)
            profile.save()
            print ("Profile " + str(profile.id) + " Created")
            profile.create_profile_init()
            print ("Profile " + str(profile.id) + " Filled")
            profile.save()
        elif Profile.objects.filter(user=user).count():
            users = User.objects.filter(first_name="Vishesh", last_name="Bangotra")
            profile = ProfileProfile.objects.filter(user=users[0])
            print ("Profile " + str(profile.id) + " Found")
            profile.save()
            profile.create_profile_init()
            print ("Profile " + str(profile.id) + " Filled")
            profile.save()
        else:
            profile = Profile(user=users[0])
            profile.save()
            print ("Profile " + str(profile.id) + " Created")
            profile.create_profile_init()
            print ("Profile " + str(profile.id) + " Filled")

        return profile

    def populate_resume(self, profile):
        print ("Profile " + str(profile.id) + " Found")
        section_names = ["Work Experience", "Education", "Interests", "Extra Curriculum", "References"]
        for section_name in section_names:
            section = Section(profile=profile, name=section_name)
            print ("Section " + str(section.name) + " Created")
            section.save()


    def handle(self, *args, **options):
        self.populate_resume(self.create_resume())

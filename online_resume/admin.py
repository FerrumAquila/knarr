from django.contrib import admin
from online_resume.models import *
from django.contrib.auth.models import User


class profileAdmin(admin.ModelAdmin):
    fieldsets = [
        ('Profile', {'fields': ['user']}),
        ('Raw Data', {'fields': ['data']}),
    ]
    list_display = ('user', )
    list_filter = ('user', )
    search_field = ('user', 'data')


class sectionAdmin(admin.ModelAdmin):
    fieldsets = [
        ('Section', {'fields': ['profile', 'name']}),
        ('Raw Data', {'fields': ['data']}),
    ]


class subSectionAdmin(admin.ModelAdmin):
    fieldsets = [
        ('Sub Section', {'fields': ['section', 'name', 'start_date', 'end_date', 'designation']}),
        ('Raw Data', {'fields': ['data']}),
    ]


class itemAdmin(admin.ModelAdmin):
    fieldsets = [
        ('Sub Section', {'fields': ['sub_section', 'name', ]}),
        ('Raw Data', {'fields': ['data']}),
    ]

admin.site.register(Profile, profileAdmin)
admin.site.register(Section, sectionAdmin)
admin.site.register(SubSection, subSectionAdmin)
admin.site.register(Item, itemAdmin)
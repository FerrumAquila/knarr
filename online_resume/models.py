from django.db import models
from django.db.models import Count
from django.conf import settings
from django.http import Http404
from django.contrib.auth.models import User
from datetime import *
from random import *
import json


class TrackingFields(models.Model):
    """
    Each model will have these fields
    """
    active = models.BooleanField(default=True)
    created_at = models.DateTimeField(auto_now_add=True)
    modified_at = models.DateTimeField(auto_now=True)

    class Meta:
        abstract = True


class Profile(TrackingFields):
    user = models.ForeignKey(User, related_name='user_profile')
    data = models.TextField(blank=True)

    def __unicode__(self):
        return self.user.first_name + "'s Profile"

    def create_profile_init(self, mobile="+919819373623", email="visheshbangotra@gmail.com", flat_no="1301", building="Primrose", society="Rachna Gardens", landmark="Mulund Darshan Building", area="Mulund Colony", city="Mumbai", pincode="400082"):
        mob_num = MobileNumber(profile=self, name="Mobile", number=mobile)
        mob_num.save()
        email_id = EmailID(profile=self, name="Email", email=email)
        email_id.save()
        address = Address(profile=self)
        address.save()
        address.add_line(line_name="flatNo", line_data=flat_no)
        address.add_line(line_name="buildingName", line_data=building)
        address.add_line(line_name="societyName", line_data=society)
        address.add_line(line_name="nearestLandmark", line_data=landmark)
        address.add_line(line_name="area", line_data=area)
        address.add_line(line_name="city", line_data=city)
        address.add_line(line_name="pincode", line_data=pincode)


class MobileNumber(TrackingFields):
    profile = models.ForeignKey(Profile, related_name='mobile_numbers')
    name = models.CharField(max_length=16, null=True, blank=True)
    number = models.CharField(max_length=13, null=True, blank=True)

    def __unicode__(self):
        return self.name + " : " + self.number


class EmailID(TrackingFields):
    profile = models.ForeignKey(Profile, related_name='email_ids')
    name = models.CharField(max_length=16, null=True, blank=True)
    email = models.EmailField(null=True, blank=True)

    def __unicode__(self):
        return self.name + " : " + self.email


class Address(TrackingFields):
    profile = models.ForeignKey(Profile, related_name='addresses')
    name = models.CharField(max_length=16, null=True, blank=True)
    data = models.TextField(blank=True)

    def __unicode__(self):
        return self.profile.user.first_name + "'s Address"

    def add_line(self, line_name, line_data):
        try:
            dataJSON = json.loads(self.data)
        except:
            dataJSON = {}

        dataJSON.update({
            line_name: line_data,
        })
        self.data = json.dumps(dataJSON, sort_keys=True)
        self.save()
        response = {
            "success": {
                line_name: dataJSON[line_name]
            }
        }

        return response

    def set_line(self, line_name, line_data):
        try:
            dataJSON = json.loads(self.data)
        except:
            dataJSON = {}

        if dataJSON[line_name]:
            dataJSON.update({
                line_name: line_data,
            })
            self.data = json.dumps(dataJSON, sort_keys=True)
            self.save()
            response = {
                "success": {
                    line_name: dataJSON[line_name]
                }
            }
        else:
            response = {
                "error": "Set Line for Unsuccessful - 100",
                "error_line": line_name
            }

        return response

    def get_line(self, line_name):
        try:
            dataJSON = json.loads(self.data)
        except:
            dataJSON = {}

        if dataJSON[line_name]:
            response = {
                "success": {
                    line_name: dataJSON[line_name]
                }
            }
        else:
            response = {
                "error": "Get Line for Unsuccessful - 101",
                "error_line": line_name
            }

        return response

"""
class Skills(TrackingFields):
    name = models.CharField
    data = models.TextField
"""

class Section(TrackingFields):
    profile = models.ForeignKey(Profile, related_name='sections')
    name = models.CharField(max_length=63, null=True, blank=True)
    data = models.TextField(blank=True)

    def __unicode__(self):
        return self.name


class SubSection(TrackingFields):
    section = models.ForeignKey(Section, related_name='sub_sections')
    name = models.CharField(max_length=63, null=True, blank=True)
    start_date = models.DateField(null=True, blank=True)
    end_date = models.DateField(null=True, blank=True)
    designation = models.CharField(max_length=63, null=True, blank=True)
    data = models.TextField(blank=True)

    def __unicode__(self):
        return self.designation + ", " + self.name

    def add_description(self, description):
        try:
            dataJSON = json.loads(self.data)
        except:
            dataJSON = {}

        dataJSON.update({
            "description": description,
        })
        self.data = json.dumps(dataJSON, sort_keys=True)
        self.save()
        response = {
            "success": {
                "description": dataJSON["description"]
            }
        }

        return response

    def get_description(self, line_name):
        try:
            dataJSON = json.loads(self.data)
        except:
            dataJSON = {}

        if dataJSON[line_name]:
            response = {
                "success": {
                    "description": dataJSON["description"]
                }
            }
        else:
            response = {
                "error": "Get Line for Unsuccessful - 101",
                "error_line": line_name
            }

        return response

    def add_point(self, point):
        try:
            dataJSON = json.loads(self.data)
        except:
            dataJSON = {}

        if dataJSON["points"]:
            points = dataJSON["points"]
        else:
            points = []

        points.append(point)
        dataJSON.update({
            "points": points,
        })
        self.data = json.dumps(dataJSON, sort_keys=True)
        self.save()
        response = {
            "success": {
                "points": dataJSON["points"]
            }
        }

        return response

    def set_point(self, point_no, point):
        try:
            dataJSON = json.loads(self.data)
        except:
            dataJSON = {}

        if dataJSON["points"][point_no-1]:
            dataJSON["points"][point_no-1] = point
            self.data = json.dumps(dataJSON, sort_keys=True)
            self.save()
            response = {
                "success": {
                    "point": dataJSON["points"][point_no-1],
                    "point_no": [point_no-1],
                }
            }
        else:
            self.add_point(point=point)
            response = {
                "success": self.add_point(point=point)
            }

        return response

    def get_point(self, point_no):
        try:
            dataJSON = json.loads(self.data)
        except:
            dataJSON = {}

        if dataJSON["points"][point_no-1]:
            response = {
                "success": {
                    "point": dataJSON["points"][point_no-1],
                    "point_no": [point_no-1],
                }
            }
        else:
            response = {
                "error": "Get Point for Unsuccessful - 201",
                "error_line": point_no
            }

        return response


class Item(TrackingFields):
    sub_section = models.ForeignKey(SubSection, related_name='items')
    name = models.CharField(max_length=127, null=True, blank=True)
    data = models.TextField(blank=True)

    def __unicode__(self):
        return self.name

    def add_point(self, point):
        try:
            dataJSON = json.loads(self.data)
        except:
            dataJSON = {}

        if dataJSON["points"]:
            points = dataJSON["points"]
        else:
            points = []

        points.append(point)
        dataJSON.update({
            "points": points,
        })
        self.data = json.dumps(dataJSON, sort_keys=True)
        self.save()
        response = {
            "success": {
                "points": dataJSON["points"]
            }
        }

        return response

    def set_point(self, point_no, point):
        try:
            dataJSON = json.loads(self.data)
        except:
            dataJSON = {}

        if dataJSON["points"][point_no-1]:
            dataJSON["points"][point_no-1] = point
            self.data = json.dumps(dataJSON, sort_keys=True)
            self.save()
            response = {
                "success": {
                    "point": dataJSON["points"][point_no-1],
                    "point_no": [point_no-1],
                }
            }
        else:
            self.add_point(point=point)
            response = {
                "success": self.add_point(point=point)
            }

        return response

    def get_point(self, point_no):
        try:
            dataJSON = json.loads(self.data)
        except:
            dataJSON = {}

        if dataJSON["points"][point_no-1]:
            response = {
                "success": {
                    "point": dataJSON["points"][point_no-1],
                    "point_no": [point_no-1],
                }
            }
        else:
            response = {
                "error": "Get Point for Unsuccessful - 201",
                "error_line": point_no
            }

        return response
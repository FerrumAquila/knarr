__author__ = 'ironeagle'

from allauth.socialaccount.adapter import DefaultSocialAccountAdapter
from allauth.account.adapter import DefaultAccountAdapter

from allauth.utils import (import_attribute,
                     email_address_exists,
                     valid_email_or_none)
from allauth.account.utils import user_email, user_username, user_field, perform_login
import datetime
from django.http import HttpResponse
from allauth.exceptions import ImmediateHttpResponse
from allauth.socialaccount.adapter import DefaultSocialAccountAdapter
from allauth.account.app_settings import EmailVerificationMethod
from allauth.socialaccount.models import SocialAccount
from django.contrib.auth.models import User
# When account is created via social, fire django-allauth signal to populate Django User record.
from allauth.account.signals import user_signed_up
from django.dispatch import receiver

@receiver(user_signed_up)
def user_signed_up_(request, user, sociallogin=None, **kwargs):
    '''
    When a social account is created successfully and this signal is received,
    django-allauth passes in the sociallogin param, giving access to metadata on the remote account, e.g.:

    sociallogin.account.provider  # e.g. 'twitter'
    sociallogin.account.get_avatar_url()
    sociallogin.account.get_profile_url()
    sociallogin.account.extra_data['screen_name']

    See the socialaccount_socialaccount table for more in the 'extra_data' field.
    '''

    if sociallogin:
        # Extract first / last names from social nets and store on User record

        if sociallogin.account.provider == 'facebook':
            gender = sociallogin.account.extra_data.get('gender')
            if gender== "male":
                gender='m'
            elif gender=="female":
                gender='f'

            birth_date = sociallogin.account.extra_data.get('birthday')
            if birth_date:
                birth_date = datetime.datetime.strptime(birth_date, '%m/%d/%Y').strftime('%Y-%m-%d')

            if sociallogin.account.extra_data.get('location'):
                city = sociallogin.account.extra_data['location']['name']
            else:
                city = None

            profile = sociallogin.account.user
            profile.username = sociallogin.account.extra_data.get("id")
            profile.email = sociallogin.account.extra_data.get("email")
            profile.first_name = sociallogin.account.extra_data.get("first_name")
            profile.last_name = sociallogin.account.extra_data.get("last_name")
            profile.password = "facebook_login"
            profile.save()



class KeedaSocialAccountAdapter(DefaultSocialAccountAdapter):

    def pre_social_login(self, request, sociallogin):
        '''if user is already in database then login
        else check if the email id for user is already in database.
        If yes then user has already used this email id to login via some other method.
        In this case connect the new account to the existing account of user and log him in without asking for any further details.
        '''
        user = sociallogin.account.user
        if user.id:
            return
        try:
            existing_user = User.objects.get(email=user.email)
            sociallogin.connect(request, existing_user)
            response = HttpResponse("hurray.")
        except User.DoesNotExist:
            pass
        else:
            perform_login(request, existing_user, EmailVerificationMethod.NONE)



__author__ = 'ironeagle'

from django.shortcuts import render
from django.shortcuts import redirect
from django.http import *
from django.contrib.auth import logout, login
from django.contrib.auth.decorators import login_required
from django import forms
from django.contrib.auth import authenticate
from itertools import chain
from datetime import *
import json
from django.utils import timezone
from random import randint
from django.core import serializers
from itertools import takewhile
from django.core.mail import send_mail
from django.core.mail import EmailMultiAlternatives
from django.core.mail import EmailMessage
from keeda.models import *
from allauth.socialaccount.models import SocialAccount


def home(request):
    context = {}
    #print("home")

    fb_account = SocialAccount.objects.filter(user__id=request.user.id, provider='facebook')
    if fb_account:
        profile_dp = "http://graph.facebook.com/{}/picture?width=60&height=60".format(fb_account[0].uid)
        profile_id = fb_account[0].extra_data["id"]
        #print profile_id
        context.update({
            "profile_dp": profile_dp,
            "profile_id": profile_id,
            "fb_account": fb_account,
        })
    else:
        return redirect("landing_page")

    return render(request, "home.html", context)


def landing_page(request):
    context = {}
    #print("Landing Page")

    return render(request, "landing_page.html", context)